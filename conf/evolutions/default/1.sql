# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table contact (
  id_contact                bigint auto_increment not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  company_name              varchar(255),
  email                     varchar(255),
  phone                     integer,
  user_id                   bigint,
  constraint pk_contact primary key (id_contact))
;

create table user (
  id                        bigint auto_increment not null,
  login                     varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table contact;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

