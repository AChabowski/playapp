package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contact extends Model{

    @Id
    public Long id_contact;

    @Constraints.Required
    public String firstName;

    @Constraints.Required
    public String lastName;

    @Constraints.Required
    public String companyName;


    public String email;


    public Integer phone;

    public Long user_id;

    private static List<Contact> contacts;

    public static Finder<Long, Contact> find = new Finder<Long, Contact>(Long.class, Contact.class);

    public Contact(Long id_contact, String firstName, String lastName,
                   String companyName, String email, Integer phone) {
        this.id_contact = id_contact;
        this.firstName = firstName;
        this.lastName = lastName;
        this.companyName = companyName;
        this.email = email;
        this.phone = phone;
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Contact: " +
                  firstName + '\''+ lastName;
    }

    public static List<Contact> findAll() {

        return find.all();
    }



    public static List<Contact> findByFirstName(String firstName) {
        final List<Contact> results = new ArrayList<Contact>();
        for (Contact candidate : contacts) {
            if (candidate.firstName.toLowerCase().contains(firstName.toLowerCase())) {
                results.add(candidate);
            }
        }

        return results;
    }

    public static Contact findById_Contact(Long id_contact) {
        for (Contact candidate : contacts) {
            if (candidate.id_contact.equals(contacts)) {
                return candidate;
            }
        }
        return null;
    }

    public static boolean remove(Contact contact) {
        return contacts.remove(contact);
    }

    public static void save(Contact contact) {
        contact.save();
    }

}
