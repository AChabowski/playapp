package models;

import java.util.*;
import java.lang.*;

import play.data.validation.Constraints;
import play.db.ebean.*;
import play.mvc.Http;
import controllers.Session;

import javax.persistence.*;

@Entity
public class User extends Model {

    @Id
    public Long id;

    @Constraints.Required
    public String login;

    @Constraints.Required
    public String email;

    @Constraints.Required
    public String password;



    public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

    public static List<User> all() {
        return find.all();
    }

    public static void create(User user) {
        user.password = Session.encryptPassword(user.password);
        user.save();
    }

    public static User authenticate (String login, String password){
        User user = find.where().eq("login", login).findUnique();
        System.out.println("ttttttuuuuutttaajjj");
//        if (Session.checkPassword(password, user.password)) return user;
        System.out.println("wyszlo");
        return user;
//        return null;
    }



    public static void delete(Long id) {
        findById(id).delete();
    }

    public void update(User user) {
        user.password = Session.encryptPassword(user.password);
        user.update(this.id);
    }

    public static User finByLogin(String login) {
        return find.where().eq("login", login).findUnique();
    }

    public static User findById(Long id) {

        return find.ref(id);
    }


}
