package controllers;


import org.mindrot.jbcrypt.BCrypt;


import play.data.validation.ValidationError;
import play.mvc.*;
import play.data.*;
import static play.data.Form.*;
import models.*;

import java.util.ArrayList;
import java.util.List;


public class Session extends Controller{

    public static class Login {

        public String login;
        public String password;

        public String validate() {
           if (User.authenticate(login,password) == null){
                return "Invalid user or password";
            }
            return null;
        }
    }

    public static String encryptPassword (String cleanPassword){
        if (cleanPassword == null) return null;
        else return BCrypt.hashpw(cleanPassword, BCrypt.gensalt());
    }
// (candidate , encrypted)
    public static boolean checkPassword (String candidate, String encrypted){
        if (candidate == null || encrypted == null) return  false;
        return BCrypt.checkpw(candidate,encrypted);
    }

    public static Result login(){

        return ok(views.html.login.render(flash("message"), form(Login.class)));
    }

    public static Result authenticate() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        if(loginForm.hasErrors()) {
            return redirect(routes.Session.login());
        } else {
            session("connected", loginForm.get().login);
            return redirect(routes.Application.index());
        }
    }

    public static Result logout (){
        session().clear();
        flash("message", "You've been logged out");
        return redirect(routes.Application.index());
    }

    public static boolean isSessionOwner (User user){
        if (user.login.equals(session("connected")))
            return true;
        else return false;
    }


}
