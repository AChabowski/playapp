package controllers;


import play.data.*;
import play.db.DB;
import play.mvc.*;

import models.*;


public class Application extends Controller {


    javax.sql.DataSource db = DB.getDataSource();

    public static Form<User> userForm = Form.form(User.class);
    public static Form<User> loginForm = Form.form(User.class);
    public static Form<Contact> contactForm = Form.form(Contact.class);


    public static Result index() {
        String flash = flash("message");
        return ok(views.html.index.render(flash, User.all()));
    }

    public static Result show(Long id) {
        User user = User.find.ref(id);
        String flash = flash("message");
        return ok(views.html.show.render(flash, user));
    }

    public static Result register(){

        return ok(views.html.register.render(userForm));
    }

    public static Result newUser (){
        Form<User> filledForm = userForm.bindFromRequest();
        if (filledForm.hasErrors()){
            flash("message", "Sorry, something went wrong. Please try again.");
            return redirect(routes.Application.index());
        } else {
            User.create(filledForm.get());
            session("connected", filledForm.data().get("login"));
            flash("message", "User was successfully created.");
            return redirect (routes.Application.show(filledForm.get().id));
        }
    }

    public static Result edit(Long id) {
        User user = User.findById(id);
        if (Session.isSessionOwner(user)) {
            return ok(views.html.edit.render(userForm.fill(user), user));
        } else {
            flash("message", "You may not edit a farmer that is not you.");
            return redirect(routes.Application.index());
        }
    }

    public static Result update(Long id) {
        Form<User> filledForm = userForm.bindFromRequest();
        if(filledForm.hasErrors()) {
            User user = User.findById(id);
            flash("message", "Sorry, we were unable to update your information. Please try again.");
            return badRequest(views.html.edit.render(userForm.fill(user), user));
        } else {
            filledForm.get().update(id);
            flash("message", "Farmer was successfully updated.");
            return redirect(routes.Application.index());
        }
    }

}

