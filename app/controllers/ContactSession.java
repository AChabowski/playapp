package controllers;


import models.Contact;
import play.data.Form;
import play.mvc.*;
import views.html.ViewsofContacts.contactsList;
import views.html.ViewsofContacts.details;


import java.util.List;

import static controllers.Application.contactForm;


public class ContactSession extends Controller {




    public static Result listContacts(){
        List<Contact> contacts = Contact.findAll();
        return ok(contactsList.render(contacts));
    }

    public static Result addContact(){
        return ok(details.render(contactForm));
    }

    public static Result deleteContact(){
        return TODO;
    }

    public static Result saveContact(){
        Form<Contact> boundForm = contactForm.bindFromRequest();
        if(boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        } else {
            Contact contact = boundForm.get();
            contact.save();
            flash("success", String.format("Successfully added product %s", contact));
        }
        return redirect(routes.ContactSession.listContacts());
    }

    public static Result editContact(String ean){
        return TODO;
    }
}
