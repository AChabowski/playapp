name := "PlayApp"

version := "1.0-SNAPSHOT"


libraryDependencies ++= {
  val liftVersion = "2.5.1"
  Seq(
    javaJdbc,
    javaEbean,
    "net.liftweb"       %% "lift-webkit"        % liftVersion        % "compile",
    "net.liftweb"       %% "lift-mapper"        % liftVersion        % "compile",
    "mysql" % "mysql-connector-java" % "5.1.18",
    "org.mindrot" % "jbcrypt" % "0.3m",
    cache
  )
}

play.Project.playJavaSettings